'use strict';

function reset_form() {
  $('#error-message').hide();
  $('#search-form input[type="text"]').val('');
}

function display_error(message) {
  $('#error-message').hide();
  $('#error-message p').text(message);
  $('#error-message').slideDown(500);
}

$(document).ready(function() {
  PhotowallHelper.display(Configuration.photowall_images);

  $('#search-button').click(function(event) {
    var vehicle_registration = $('#vehicle-registration').val();
    var vehicle_registration_validation = new RegExp($('#vehicle-registration').attr('pattern'));

    if (!vehicle_registration_validation.test(vehicle_registration)) {
      display_error($('#vehicle-registration').attr('title'));

      event.stopPropagation();
      return false;
    }

    var stock_reference = $('#stock-reference').val();
    var stock_reference_validation = new RegExp($('#stock-reference').attr('pattern'));

    if (!stock_reference_validation.test(stock_reference)) {
      display_error($('#stock-reference').attr('title'));

      event.stopPropagation();
      return false;
    }

    var obfuscated_stock_reference = ArnoldClarkHelper.build_obfuscated_stock_reference(vehicle_registration, stock_reference);

    var images = ArnoldClarkHelper.build_images(obfuscated_stock_reference);

    GalleryHelper.display(images, '#vehicle-images');

    reset_form();
  });
});
