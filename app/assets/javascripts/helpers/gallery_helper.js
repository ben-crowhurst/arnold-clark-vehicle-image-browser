'use strict';

var GalleryHelper = {
  display: function(photos, container_id) {
    $(container_id + ' a').remove();
    $(container_id).append(photos);

    var settings = {
      theme: 'light',
      tooltip: false,
      pluginPath: Configuration.zoomscript_plugin_path,
      monitor: {
        thumbs: {
          position: 'left'
        }
      }
    };

    setTimeout(function() {
        $(container_id + ' .image').zoomscript(settings);
        $(container_id).find('img:first').trigger("click");
    }, 500);
  }
};
