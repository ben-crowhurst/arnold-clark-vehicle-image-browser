'use strict';

var ImageHelper = {
  preload: function(url) {
    var featured = new Image();
    featured.onerror = ErrorHelper.featured_error_handler;
    featured.src = url;
  }
};
