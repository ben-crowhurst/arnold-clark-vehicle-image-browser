'use strict';

var UrlFactory = {
  build: function(domain, path) {
    if (ValidationHelper.is_null_or_empty(domain)) {
      domain = '';
    } else {
      var length = domain.length - 1;
      domain = (domain[length] === '/') ? domain.substr(0, length) : domain;
    }

    if (ValidationHelper.is_null_or_empty(path)) {
      path = '';
    } else {
      path = (path[0] !== '/') ? '/' + path : path;
    }

    return domain + path;
  }
};
