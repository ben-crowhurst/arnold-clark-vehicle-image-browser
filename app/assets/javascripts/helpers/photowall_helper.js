'use strict';

var PhotowallHelper = {
  display: function(images) {
    PhotoWall.init({
      el: '#photowall',

      zoom: false,
      zoomTimeout: 100,
      zoomDuration: 100,
      zoomImageBorder: 5,

      showBox: false,
      showBoxSocial: false,

      lineMaxHeight: 150,
      lineMaxHeightDynamic: true,
    });

    var photos = {};

    for (var index = 0; index != images.length; index++) {
      photos[index] = {
        id: index,
        img: images[index],
        th: {
          src: images[index],
        }
      };
    }

    PhotoWall.load(photos);
  }
};
