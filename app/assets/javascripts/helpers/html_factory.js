'use strict';

var HtmlFactory = {
  build_anchor_element: function(image_uri, innerHtml) {
    if (ValidationHelper.is_null_or_empty(image_uri)) {
        return '';
    }

    if (ValidationHelper.is_null_or_empty(innerHtml)) {
      innerHtml = '';
    }

    return '<a href="' + image_uri + '" class="image">' + innerHtml + '</a>';
  },
  build_image_element: function(image_uri, image_id) {
    if (ValidationHelper.is_null_or_empty(image_uri)) {
      return '';
    }

    if (ValidationHelper.is_null_or_empty(window.thumbnail_error_handler)) {
      window.thumbnail_error_handler = ErrorHelper.thumbnail_error_handler;
    }

    image_id = (ValidationHelper.is_null_or_empty(image_id)) ? '' : this.build_html4_safe_image_id(image_id);

    return '<img id="' + image_id +'" src="' + image_uri +'" onerror="window.thumbnail_error_handler(this);" />';
  },
  build_html4_safe_image_id: function(image_id) {
    if (ValidationHelper.is_null_or_empty(image_id)) {
      return '';
    }

    image_id = image_id.replace(/[^a-z0-9\-_\.:]/gi, '-');

    if (/[^a-zA-Z]/.test(image_id[0])) {
      image_id = image_id.substring(1);
    }

    return image_id;
  }
};
