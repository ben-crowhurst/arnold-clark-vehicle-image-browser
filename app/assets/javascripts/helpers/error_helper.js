'use strict';

var ErrorHelper = {
  thumbnail_error_handler: function(image) {
    var images = $('img[src="' + image.src + '"]');

    for (var index = 0; index != images.length; index++) {
      images[index].src = '/images/default.png';
    }
  },
  featured_error_handler: function() {
    var image = this;
    var anchors = $('a[href="' + image.src + '"]');

    for (var index = 0; index != anchors.length; index++) {
      anchors[index].href = '/images/default.png';
    }
  }
};
