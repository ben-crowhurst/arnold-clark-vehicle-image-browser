'use strict';

var ValidationHelper = {
  is_null_or_empty: function(value) {
    return (value === null || value === undefined || value == '');
  }
}
