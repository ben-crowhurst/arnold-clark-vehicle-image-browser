'use strict';

var ArnoldClarkHelper = {
  build_obfuscated_stock_reference: function(registration_plate, stock_reference) {
    if (ValidationHelper.is_null_or_empty(stock_reference) ||
        ValidationHelper.is_null_or_empty(registration_plate)) {
      return '';
    }

    if (stock_reference.length < 9) {
      return ''
    }

    registration_plate = registration_plate.replace(/ /g,'');
    registration_plate = registration_plate.split('').reverse();

    stock_reference = stock_reference.replace(/ /g,'');
    stock_reference = stock_reference.split('');

    var obfuscated_reference = $.map(registration_plate, function(value, index) {
                                 return [value, stock_reference[index + 1]];
                               });

    obfuscated_reference.unshift(stock_reference[0]);
    obfuscated_reference.pop();
    obfuscated_reference.push(stock_reference[8]);

    return obfuscated_reference.join('');
  },
  build_images: function(obfuscated_stock_reference) {
    var camera_angles = Configuration.camera_angles;

    var images = [];

    for (var index = 0; index != camera_angles.length; index++) {
        var thumbnail_image_id = obfuscated_stock_reference + '/350/' + camera_angles[index];

        var thumbnail_image_url = UrlFactory.build(Configuration.image_server_url, thumbnail_image_id);

        var thumbnail = HtmlFactory.build_image_element(thumbnail_image_url, thumbnail_image_id);

        var featured_image_id = obfuscated_stock_reference + '/800/' + camera_angles[index];
        var featured_image_url = UrlFactory.build(Configuration.image_server_url, featured_image_id);

        ImageHelper.preload(featured_image_url);

        images.push(HtmlFactory.build_anchor_element(featured_image_url, thumbnail));
    }

    return images;
  }
}
