'use strict';

var Configuration = {
  camera_angles: ['r', 'i', '6', 'f', '4', '5'],
  zoomscript_plugin_path: '/assets/zoomscript',
  image_server_url: 'http://imagecache.arnoldclark.com/imageserver',
  photowall_images: ['/images/fiat-logo.png',
                      '/images/citroen-logo.png',
                      '/images/ford-logo.png',
                      '/images/hyundai-logo.png',
                      '/images/masda-logo.png',
                      '/images/mercedes-benz-logo.png',
                      '/images/peugoet-logo.png',
                      '/images/renault-logo.png',
                      '/images/vauxhall-logo.png',
                      '/images/volkswagen-logo.png',
                      '/images/toyota-logo.png',
                      '/images/smart-logo.png',
                      '/images/skoda-logo.png',
                      '/images/seat-logo.png',
                      '/images/nissan-logo.png',
                      '/images/mini-logo.png',
                      '/images/kia-logo.png',
                      '/images/jeep-logo.png',
                      '/images/honda-logo.png',
                      '/images/chrysler-logo.png',
                      '/images/chevrolet-logo.png',
                      '/images/bmw-logo.png']
};
