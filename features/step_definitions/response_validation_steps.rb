Then(/^I should see the vehicle image search homepage$/) do
  expect(page).to have_title 'Arnold Clark Vehicle Image Search'
end

Then(/^I should see a text input box for "(.*?)"$/) do |field_name|
  field_name = field_name.tr(' ', '-')
  expect(page).to have_css("input[id=\"#{field_name}\"]")
end

Then(/^I should see a "(.*?)" button$/) do |button_name|
  button_name = button_name.tr(' ', '-')
  expect(page).to have_selector(:link_or_button, button_name)
end

Then(/^I should see a collection of (\d+) vehicle images$/) do |count|
  within '#vehicle-images' do
    page.assert_selector('img', count: count)
  end
end

Then(/^I should see the following error message "(.*?)"$/) do |message|
    expect(page).to have_content message
end
