Given(/^there is an active web service at "(.*?)"$/) do |service_uri|
  response = HttpSupport.head(service_uri)

  expect(response.code).to eq('200')
end

Given(/^there is stock associated with the vehicle registration "(.*?)" and stock reference number "(.*?)"$/) do |vehicle_registration, stock_reference|
  base = 'http://imagecache.arnoldclark.com/imageserver/'

  obfuscated_reference =
    StockSupport.build_obfuscated_reference(vehicle_registration,
                                            stock_reference)

  url = base + obfuscated_reference

  camera_angles = %w(r i 6 f 4 5)

  camera_angles.each do |angle|
    response = HttpSupport.head(url + '/350/' + angle)
    expect(response.code).to eq('200')

    response = HttpSupport.head(url + '/800/' + angle)
    expect(response.code).to eq('200')
  end
end
