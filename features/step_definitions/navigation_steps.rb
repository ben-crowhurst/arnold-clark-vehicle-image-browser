When(/^I navigate to the search page$/) do
  visit Constants::SEARCH_URI
end

When(/^I click the "(.*?)" button$/) do |button_name|
  click_button(button_name)
end
