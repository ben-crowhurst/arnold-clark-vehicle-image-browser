Given(/^I fill in the "(.*?)" field with "(.*?)"$/) do |field_name, field_value|
  within '#search-form' do
    fill_in field_name, with: field_value
  end
end
