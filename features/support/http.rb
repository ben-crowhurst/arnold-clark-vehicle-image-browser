require 'uri'
require 'net/http'

# HTTP functionality
module HttpSupport
  def self.head(endpoint)
    uri = URI.parse(endpoint)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Head.new(uri.request_uri)

    response = http.request(request)

    response
  end
end
