# Stock Support
module StockSupport
  def self.build_obfuscated_reference(registration_plate, stock_reference)
    stock_reference_array = stock_reference.chars
    registration_plate_array = registration_plate.chars.reverse

    interlaced_array = []
    interlaced_array << stock_reference_array[0]

    registration_plate_array.each_with_index do |value, index|
      interlaced_array << value << stock_reference_array[index + 1]
    end

    interlaced_array[-1] = stock_reference_array[8]

    interlaced_array.join('')
  end
end
