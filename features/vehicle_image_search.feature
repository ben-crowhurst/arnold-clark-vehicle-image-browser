Feature: Vehicle image search
  In order to browse the current Arnold Clark motor vehicle stock images
  As a warehouse stock manager
  I want to be able to enter a vehicle registration and stock reference number to view all available and relevant images

  Scenario: Search page availability
    Given there is an active web service at "http://localhost:3000"
    When I navigate to the search page
    Then I should see the vehicle image search homepage
    And I should see a text input box for "vehicle registration"
    And I should see a text input box for "stock reference"
    And I should see a "Search" button

  @javascript
  Scenario: Instock search
    Given there is stock associated with the vehicle registration "PF62KMY" and stock reference number "ARNEA-U-7669"
    And I navigate to the search page
    And I fill in the "Vehicle Registration" field with "PF62KMY"
    And I fill in the "Stock Reference" field with "ARNEA-U-7669"
    When I click the "Search" button
    Then I should see a collection of 6 vehicle images

  @javascript
  Scenario: Invalid vehicle registration
    Given I navigate to the search page
    And I fill in the "Vehicle Registration" field with ""
    And I fill in the "Stock Reference" field with "ARNEA-U-7669"
    When I click the "Search" button
    Then I should see the following error message "Please enter a valid vehicle registration."

  @javascript
  Scenario: Invalid stock reference
    Given I navigate to the search page
    And I fill in the "Vehicle Registration" field with "PF62KMY"
    And I fill in the "Stock Reference" field with ""
    When I click the "Search" button
    Then I should see the following error message "Please enter a valid stock reference."
