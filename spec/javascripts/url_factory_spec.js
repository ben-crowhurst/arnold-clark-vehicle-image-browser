'use strict';

describe('UrlFactory', function() {
  it('builds url', function() {
      expect(UrlFactory.build('http://imagecache.arnoldclark.com/imageserver', 'AFRBNEA4I5-G6D3/350/r')).toEqual('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r');
  });

  it('builds url given no leading forward slash on path', function() {
    expect(UrlFactory.build('http://imagecache.arnoldclark.com/imageserver/', 'AFRBNEA4I5-G6D3/350/r')).toEqual('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r');
  });

  it('builds url given a null path', function() {
    expect(UrlFactory.build('http://imagecache.arnoldclark.com/imageserver/', null)).toEqual('http://imagecache.arnoldclark.com/imageserver');
  });

  it('builds url given a undefined path', function() {
    expect(UrlFactory.build('http://imagecache.arnoldclark.com/imageserver/', undefined)).toEqual('http://imagecache.arnoldclark.com/imageserver');
  });

  it('builds url given an empty path', function() {
    expect(UrlFactory.build('http://imagecache.arnoldclark.com/imageserver/', '')).toEqual('http://imagecache.arnoldclark.com/imageserver');
  });

 it('builds url given no lagging forward slash on domain', function() {
   expect(UrlFactory.build('http://imagecache.arnoldclark.com/imageserver', 'AFRBNEA4I5-G6D3/350/r')).toEqual('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r');
 });

 it('builds url given a null domain', function() {
   expect(UrlFactory.build(null, 'AFRBNEA4I5-G6D3/350/r')).toEqual('/AFRBNEA4I5-G6D3/350/r');
 });

 it('builds url given a undefined domain', function() {
   expect(UrlFactory.build(undefined, 'AFRBNEA4I5-G6D3/350/r')).toEqual('/AFRBNEA4I5-G6D3/350/r');
 });

 it('builds url given an empty domain', function() {
   expect(UrlFactory.build('', 'AFRBNEA4I5-G6D3/350/r')).toEqual('/AFRBNEA4I5-G6D3/350/r');
 });

 it('builds url given no leading and lagging forward slash', function() {
   expect(UrlFactory.build('http://imagecache.arnoldclark.com/imageserver/', '/AFRBNEA4I5-G6D3/350/r')).toEqual('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r');
 });
});
