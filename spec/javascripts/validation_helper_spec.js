'use strict';

describe('ValidationHelper', function() {
  it('returns true given a null argument', function() {
      expect(ValidationHelper.is_null_or_empty(null)).toBeTruthy();
  });

  it('returns true given an undefined argument', function() {
    expect(ValidationHelper.is_null_or_empty(undefined)).toBeTruthy();
  });

  it('returns true given an empty argument', function() {
    expect(ValidationHelper.is_null_or_empty('')).toBeTruthy();
  });

  it('returns false given a string argument', function() {
    expect(ValidationHelper.is_null_or_empty('SB53 YUK')).toBeFalsy();
  });

  it('returns false given a numeric argument', function() {
    expect(ValidationHelper.is_null_or_empty(53)).toBeFalsy();
  });

  it('returns false given an array argument', function() {
    expect(ValidationHelper.is_null_or_empty([1,2])).toBeFalsy();
  });

  it('returns true given an empty array argument', function() {
    expect(ValidationHelper.is_null_or_empty([])).toBeTruthy();
  });

  it('returns false given an object argument', function() {
    expect(ValidationHelper.is_null_or_empty({registration: 'SB53YUK'})).toBeFalsy();
  });

  it('returns false given an empty object argument', function() {
    expect(ValidationHelper.is_null_or_empty({})).toBeFalsy();
  });
});