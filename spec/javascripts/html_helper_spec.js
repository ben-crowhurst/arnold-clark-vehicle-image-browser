'use strict';

describe('HtmlFactory', function() {
  describe('build_image_element', function() {
    it('combines image id and image uri to create a string representation of a html image element', function() {
      expect(HtmlFactory.build_image_element('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r', 'AFRBNEA4I5-G6D3/350/r')).toEqual('<img id="AFRBNEA4I5-G6D3-350-r" src="http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r" onerror="window.thumbnail_error_handler(this);" />');
    });

    it('returns an empty string given an empty image uri', function() {
      expect(HtmlFactory.build_image_element('', 'AFRBNEA4I5-G6D3/350/r')).toEqual('');
    });

    it('returns an empty string given an null image uri', function() {
      expect(HtmlFactory.build_image_element(null, 'AFRBNEA4I5-G6D3/350/r')).toEqual('');
    });

    it('returns an empty string given an undefined image uri', function() {
      expect(HtmlFactory.build_image_element(undefined, 'AFRBNEA4I5-G6D3/350/r')).toEqual('');
    });

    it('returns a string representation of a html image element with an empty id given an empty image id argument', function() {
      expect(HtmlFactory.build_image_element('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r', '')).toEqual('<img id="" src="http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r" onerror="window.thumbnail_error_handler(this);" />');
    });

    it('returns a string representation of a html image element with an empty id given an null image id argument', function() {
      expect(HtmlFactory.build_image_element('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r', null)).toEqual('<img id="" src="http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r" onerror="window.thumbnail_error_handler(this);" />');
    });

    it('returns a string representation of a html image element with an empty id given an undefined image id argument', function() {
      expect(HtmlFactory.build_image_element('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r', undefined)).toEqual('<img id="" src="http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r" onerror="window.thumbnail_error_handler(this);" />');
    });
  });
         
  describe('build_anchor_element', function() {
    it('combines image uri and inner html to create a string representation of a html anchor element', function() {
      expect(HtmlFactory.build_anchor_element('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r', 'Arnold Clark')).toEqual('<a href="http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r" class="image">Arnold Clark</a>');
    });

    it('returns an empty string given an empty image uri', function() {
      expect(HtmlFactory.build_anchor_element('', 'Arnold Clark')).toEqual('');
    });

    it('returns an empty string given an null image uri', function() {
      expect(HtmlFactory.build_anchor_element(null, 'Arnold Clark')).toEqual('');
    });

    it('returns an empty string given an undefined image uri', function() {
      expect(HtmlFactory.build_anchor_element(undefined, 'Arnold Clark')).toEqual('');
    });

    it('returns a string representation of an empty anchor element given an empty inner html argument', function() {
      expect(HtmlFactory.build_anchor_element('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r', '')).toEqual('<a href="http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r" class="image"></a>');
    });

    it('returns a string representation of an empty anchor element given a null inner html argument', function() {
      expect(HtmlFactory.build_anchor_element('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r', null)).toEqual('<a href="http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r" class="image"></a>');
    });

    it('returns a string representation of an empty anchor element given an undefined inner html argument', function() {
      expect(HtmlFactory.build_anchor_element('http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r', undefined)).toEqual('<a href="http://imagecache.arnoldclark.com/imageserver/AFRBNEA4I5-G6D3/350/r" class="image"></a>');
    });
  });

  describe('build_html4_safe_image_id', function() {
    it('replaces all invalid forward slash characters with hyphen', function() {
      expect(HtmlFactory.build_html4_safe_image_id('AFRBNEA4I5-G6D3/350/r')).toEqual('AFRBNEA4I5-G6D3-350-r');
    });

    it('removes any leading invalid characters', function() {
      expect(HtmlFactory.build_html4_safe_image_id('/AFRBNEA4I5-G6D3/350/r')).toEqual('AFRBNEA4I5-G6D3-350-r');
    });

    it('replaces all invalid characters with hyphen', function() {
      expect(HtmlFactory.build_html4_safe_image_id('£A$FR&%B$*NE*A(4I)5-G6D3/350/r')).toEqual('A-FR--B--NE-A-4I-5-G6D3-350-r');
    });

    it('returns empty given a null argument', function() {
      expect(HtmlFactory.build_html4_safe_image_id(null)).toEqual('');
    });

    it('returns empty given a undefined argument', function() {
      expect(HtmlFactory.build_html4_safe_image_id(undefined)).toEqual('');
    });

    it('returns empty given a empty argument', function() {
      expect(HtmlFactory.build_html4_safe_image_id('')).toEqual('');
    });
  });
});
