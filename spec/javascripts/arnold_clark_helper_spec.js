'use strict';

describe('ArnoldClarkHelper', function() {
  it('combines a vehicle registration and stock reference number given a 15 letter stock reference', function() {
      expect(ArnoldClarkHelper.build_obfuscated_stock_reference('PF62KMY', 'ARNEA-U-7669')).toEqual('AYRMNKE2A6-FUP7');
  });

  it('combines a vehicle registration and stock reference number given a 3 letter registration plate', function() {
    expect(ArnoldClarkHelper.build_obfuscated_stock_reference('T22', 'ARNEA-U-7669')).toEqual('A2R2NT7');
  });

  it('combines a vehicle registration and stock reference number given a 11 letter stock reference', function() {
    expect(ArnoldClarkHelper.build_obfuscated_stock_reference('DG54EBF', 'ARNAI-64340')).toEqual('AFRBNEA4I5-G6D3');
  });

  it('combines a vehicle registration and stock reference number given a 9 letter stock reference', function() {
    expect(ArnoldClarkHelper.build_obfuscated_stock_reference('DG54EBF', 'ARNAI-643')).toEqual('AFRBNEA4I5-G6D3');
  });

  it('combines a vehicle registration and stock reference number removing irrelevant whitespace', function() {
    expect(ArnoldClarkHelper.build_obfuscated_stock_reference('  DG5 4EBF ', ' AR N AI- 6434 0  ')).toEqual('AFRBNEA4I5-G6D3');
  });

  it('returns an empty reference given a stock reference with a length less than 9', function() {
    expect(ArnoldClarkHelper.build_obfuscated_stock_reference('DG54EBF', 'ARNAI')).toEqual('');
  });

  it('returns an empty reference given only a vehicle registration', function() {
    expect(ArnoldClarkHelper.build_obfuscated_stock_reference('PF62KMY', '')).toEqual('');
  });

  it('returns an empty reference given only a stock reference', function() {
    expect(ArnoldClarkHelper.build_obfuscated_stock_reference('', 'ARNEA-U-7669')).toEqual('');
  });

  it('returns an empty reference given empty arguments', function() {
    expect(ArnoldClarkHelper.build_obfuscated_stock_reference('', '')).toEqual('');
  });

  it('returns an empty reference given null arguments', function() {
    expect(ArnoldClarkHelper.build_obfuscated_stock_reference(null, null)).toEqual('');
  });

  it('returns an empty reference given undefined arguments', function() {
    expect(ArnoldClarkHelper.build_obfuscated_stock_reference(undefined, undefined)).toEqual('');
  });
});
